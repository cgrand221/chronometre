



public class DefilementTemps extends Thread {
	private Chrono ch;
	private int j;
	private int h;
	private int m;
	private int s;
	private int ml;




	private long ref;
	private long refPause;
	private boolean attente;


	public DefilementTemps(Chrono c) {
		this.ch = c;

	}

	public void run() {

		super.run();

		j = 0;
		h = 0;
		m = 0;
		s = 0;
		ml = 0;





		ref = System.currentTimeMillis();
		long temps;
		while (true){
			ch.setHorloge();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			
			if (this.attente){
				attente();
			}
			temps = System.currentTimeMillis() - ref;


			
			ml = (int) (temps%1000);
			temps=temps/1000;
			s = (int) (temps%60);
			temps=temps/60;
			m = (int) (temps%60);
			temps=temps/60;
			h = (int) (temps%24);
			temps=temps/24;
			j = (int) temps;
			getTemps();
			//System.err.println("r");
		}
	}
	
	synchronized void reprise(){
		notify();
	}
	
	synchronized void attente(){
		try {
			wait();
		} catch (InterruptedException e) {
			;
		}
	}
	public void suspendre() {
		refPause = System.currentTimeMillis();		
		this.attente = true;
		



	}
	public void reprendre(){

		ref += System.currentTimeMillis()-refPause;
		this.attente = false;
		reprise();
	}

	public void getTemps() {		
		int mlTmp = ml;
		int sTmp = s;
		int mTmp = m;
		int hTmp = h;
		int jTmp = j;
		if (mlTmp < 0){
			mlTmp+= 1000;
			sTmp--;
		}
		if (sTmp < 0){
			sTmp+= 60;
			mTmp--;
		}
		if (mTmp < 0){
			mTmp+= 60;
			hTmp--;
		}
		if (hTmp < 0){
			hTmp += 24;
			jTmp--;
		}
		if (jTmp < 0){
			jTmp+= 1000;
		}
		/*atomiquement :*/
		this.ch.ml = mlTmp;
		this.ch.s = sTmp;
		this.ch.m = mTmp;
		this.ch.h = hTmp;
		this.ch.j = jTmp;
		/*fin atomiquement*/
	}

}
