import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;


public class Chrono implements ActionListener {
	private static final int HAUTEUR = 400;
	private static final int LARGEUR = 600;
	Font POLICE = new Font("Arial",Font.PLAIN,40);
	private JButton startStop;
	private JButton resetNext;
	private DefaultListModel dlmTempsTours;
	private DefaultListModel dlmTempsSomme;
	private boolean start;
	private boolean reset;
	private DefilementTemps th;
	private JLabel texteChrono;
	private JFrame f;
	public int ml;
	public int s;
	public int m;
	public int h;
	public int j;
	private int jPrec;
	private int hPrec;
	private int mPrec;
	private int sPrec;
	private int mlPrec;
	private JScrollPane jsp;
	public Chrono() {
		th = new DefilementTemps(this);
		this.start = false;
		this.reset = true;
		f = new JFrame("000J 00:00:00,00");
		JPanel panneau = new JPanel();
		panneau.setLayout(new BorderLayout());
		JPanel panneau2 = new JPanel();
		panneau2.setLayout(new GridLayout(1,1));
		texteChrono = new JLabel("000J 00:00:00,00");
		texteChrono.setFont(POLICE);
		panneau2.add(texteChrono);


		JPanel panneau3 = new JPanel();
		panneau3.setLayout(new GridLayout(1,2));
		startStop = new JButton("START/STOP");
		startStop.addActionListener(this);
		resetNext = new JButton("RESET/NEXT");
		resetNext.addActionListener(this);
		panneau3.add(startStop);
		panneau3.add(resetNext);

		JPanel panneau4 = new JPanel();
		
		panneau4.setLayout(new BorderLayout());

		JPanel panneau5 = new JPanel();
		JPanel panneau6 = new JPanel();


		panneau5.setLayout(new GridLayout(1,2));
		panneau6.setLayout(new GridLayout(1,2));

		panneau5.add(new JLabel("temps Tour"),BorderLayout.NORTH);
		panneau5.add(new JLabel("temps Somme"),BorderLayout.NORTH);

		dlmTempsTours = new DefaultListModel();
		JList tempsTour = new JList(dlmTempsTours);		
		
		panneau6.add(tempsTour,BorderLayout.CENTER);


		dlmTempsSomme = new DefaultListModel();
		JList tempsSomme = new JList(dlmTempsSomme);		
		
		panneau6.add(tempsSomme,BorderLayout.CENTER);


		panneau4.add(panneau5,BorderLayout.NORTH);
		jsp = new JScrollPane(panneau6);
		panneau4.add(jsp,BorderLayout.CENTER);



		panneau.add(panneau2,BorderLayout.NORTH);
		panneau.add(panneau4,BorderLayout.CENTER);
		panneau.add(panneau3,BorderLayout.SOUTH);
		f.add(panneau);
		f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		f.addWindowListener(new EcouteurFenetre());
		f.setSize(new Dimension(LARGEUR,HAUTEUR));
		jPrec = 0;
		hPrec = 0;
		mPrec = 0;
		sPrec = 0;
		mlPrec = 0;
		dlmTempsSomme.addElement(chaine(j,h,m,s,ml));
		dlmTempsTours.addElement(chaine(j,h,m,s,ml));
		f.setVisible(true);

	}

	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == this.startStop){
			if (this.reset){
				this.start = true;
				this.reset = false;
				th.start();
			}
			else{
				if (this.start){
					this.start = false;
					th.suspendre();
				}
				else{
					this.start = true;
					th.reprendre();
				}
			}
		}
		else if (ae.getSource() == this.resetNext){
			if (this.start){
				th.getTemps();		
				
				dlmTempsSomme.addElement(chaine(j,h,m,s,ml));
				dlmTempsTours.addElement(calculTempsTour());
				
				JScrollBar bar = jsp.getVerticalScrollBar();
				bar.setValue(bar.getMaximum());	
				
				this.jPrec = this.j;
				this.hPrec = this.h;
				this.mPrec = this.m;
				this.sPrec = this.s;
				this.mlPrec = this.ml;
			}
			else if (this.reset){
				dlmTempsSomme.removeAllElements();
				dlmTempsTours.removeAllElements();
				dlmTempsSomme.addElement(chaine(j,h,m,s,ml));
				dlmTempsTours.addElement(chaine(j,h,m,s,ml));
			}
			else {
				this.reset = true;
				this.jPrec = 0;
				this.hPrec = 0;
				this.mPrec = 0;
				this.sPrec = 0;
				this.mlPrec = 0;
				j = 0;
				h = 0;
				m = 0;
				s = 0;
				ml = 0;
				dlmTempsSomme.addElement(chaine(j,h,m,s,ml));
				dlmTempsTours.addElement(chaine(j,h,m,s,ml));
				th = null;
				th = new DefilementTemps(this);
				th.getTemps();
				setHorloge();
			}
		}

	}

	private String calculTempsTour() {
		int ml = this.ml-this.mlPrec;
		int s = this.s-this.sPrec;
		int m = this.m-this.mPrec;
		int h = this.h-this.hPrec;
		int j = this.j-this.jPrec;
		
		if (ml < 0){
			ml+=1000;
			s--;
		}
		
		if (s < 0){
			s+=60;	
			m--;
		}
		
		if (m < 0){
			m+=60;
			h--;
		}
		if (h < 0){
			h+=24;
			j--;
		}
		return chaine(j, h, m, s, ml);
		
	
	}

	public String chaine(int j, int h, int m, int s, int ml){
		String jj,hh,mm,ss,cc;
		if (j > 99){
			jj = j+"J ";
		}
		else if (j > 99){
			jj = "0"+j+"J ";
		}
		else jj = "00"+j+"J ";
		if (h > 9)
			hh = h+":";
		else hh = "0"+h+":";
		if (m > 9)
			mm = m+":";
		else mm = "0"+m+":";
		if (s > 9)
			ss = s+",";
		else ss = "0"+s+",";
		if (ml>99)
			cc = ""+(ml/10);
		else cc = "0"+(ml/10);
		
		return jj+hh+mm+ss+cc;
	}
	
	public void setHorloge() {
		String tmp = chaine(j,h,m,s,ml);
		texteChrono.setText(tmp);
		dlmTempsTours.setElementAt(calculTempsTour(), dlmTempsTours.getSize()-1);
		dlmTempsSomme.setElementAt(chaine(j,h,m,s,ml), dlmTempsSomme.getSize()-1);
		
		f.setTitle(tmp);
	}
}
